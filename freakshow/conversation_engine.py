#! /home/loic/.virtualenvs/freakshow/bin/python

import speech_recognition as sr
import mlconjug

from random import choice
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types
from answers import *

client = language.LanguageServiceClient()
conjugator = mlconjug.Conjugator(language='fr')


def get_verb_tense(token):
    tense = enums.PartOfSpeech.Tense(token.part_of_speech.tense)

    if tense == enums.PartOfSpeech.Tense.FUTURE:
        return ('Indicatif', 'Futur')
    if tense == enums.PartOfSpeech.Tense.CONDITIONAL_TENSE:
        return ('Conditionnel', 'Présent')
    if tense == enums.PartOfSpeech.Tense.PAST:
        return ('Indicatif', 'Passé simple')
    if tense == enums.PartOfSpeech.Tense.IMPERFECT:
        return ('Indicatif', 'Imparfait')
    if tense == enums.PartOfSpeech.Tense.PLUPERFECT:
        return ('Indicatif', 'Plus-que-parfait')

    return ('Indicatif', 'Présent')


def get_answer(question_word):
    if question_word == QuestionWord.UNKNOWN:
        return None

    if question_word == QuestionWord.QUI:
        return choice(ANSWERS_QUI)
    if question_word == QuestionWord.QUOI:
        return choice(ANSWERS_QUOI)
    if question_word == QuestionWord.OU:
        return choice(ANSWERS_OU)
    if question_word == QuestionWord.QUAND:
        return choice(ANSWERS_QUAND)
    if question_word == QuestionWord.COMMENT:
        return choice(ANSWERS_COMMENT)
    if question_word == QuestionWord.QUEL:
        return choice(ANSWERS_QUEL)
    if question_word == QuestionWord.QUELLE:
        return choice(ANSWERS_QUELLE)
    if question_word == QuestionWord.COMBIEN:
        return choice(ANSWERS_COMBIEN)
    if question_word == QuestionWord.CLOSED:
        return choice(ANSWERS_CLOSED)


def speech_recognition(recognizer, audio):
    answer = None
    try:
        text = recognizer.recognize_google_cloud(
            audio,
            language="fr_FR")
        print("-> {}".format(text))
        answer = analyze_query(text)
        print("<- {}".format(answer))
        # print_ticket(answer)

    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Google error : {0}".format(e))

    return answer


def analyze_query(text):
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT,
        language="fr_FR")

    tokens = client.analyze_syntax(document).tokens
    question_word = None

    if (len(tokens) > 4 and
            tokens[0].text.content == 'est' and
            tokens[2].text.content == 'ce' and
            tokens[3].text.content in ('que', 'qu\'')):
        partial_answer = get_answer(QuestionWord.CLOSED)
        answer = []
        should_change_person = False

        for idx, token in enumerate(tokens[4:]):
            tag = enums.PartOfSpeech.Tag(token.part_of_speech.tag)
            label = enums.DependencyEdge.Label(token.dependency_edge.label)
            person = enums.PartOfSpeech.Person(token.part_of_speech.person)
            tense = enums.PartOfSpeech.Tense(token.part_of_speech.tense)

            if (person == enums.PartOfSpeech.Person.FIRST and
                    tag == enums.PartOfSpeech.Tag.PRON):
                answer.append(PERSON_TRANSLATION_TABLE[token.text.content])
                should_change_person = True
            elif (tag == enums.PartOfSpeech.Tag.VERB and
                    label == enums.DependencyEdge.Label.ROOT):
                if (should_change_person):
                    mode, tense = get_verb_tense(token)
                    answer.append(conjugator.conjugate(token.lemma).conjug_info[mode][tense]['2s'])
                else:
                    answer.append(token.text.content)

                if partial_answer['is_positive'] is False:
                    answer.append('pas')

            else:
                answer.append(token.text.content)

        return "{} {}".format(
            partial_answer['answer'],
            ' '.join(answer))
    else:
        # should detect basic question word
        question_word = QuestionWord.from_str(tokens[0].lemma)
        return get_answer(question_word)
