#! /home/loic/.virtualenvs/freakshow/bin/python

import speech_recognition as sr
import hyphen
import conversation_engine


from textwrap2 import fill
from escpos import *
from alsa_error_handler import noalsaerr


PRINTER_DEVFILE = "/dev/ttyUSB0"
TICKET_HEADER = ("╔═══╗ Φ ╔═══╗ Φ ╔═══╗ Φ ╔═══╗ Φ ╔═══╗\n"
                 "║ Φ ╚═══╝ Φ ╚═══╝ Φ ╚═══╝ Φ ╚═══╝ Φ ║\n")
TICKET_FOOTER = ("║ Φ ╔═══╗ Φ ╔═══╗ Φ ╔═══╗ Φ ╔═══╗ Φ ║\n"
                 "╚═══╝ Φ ╚═══╝ Φ ╚═══╝ Φ ╚═══╝ Φ ╚═══╝\n")

hyphenator = hyphen.Hyphenator("fr_FR")


def main():
    with_mic = False

    if with_mic:
        try:
            with noalsaerr():
                for idx, name in enumerate(
                        sr.Microphone.list_microphone_names()):
                    print("{0:2}) {1}".format(idx, name))

                device_index = int(input("Microphone index : "))
                r = sr.Recognizer()
                m = sr.Microphone(device_index=device_index)

                print("A moment of silence, please...")
                with m as source:
                    r.adjust_for_ambient_noise(source)
                print("Set minimum energy threshold to {}".format(
                    r.energy_threshold))

                while True:
                    print("Ask something!")
                    with m as source:
                        audio = r.listen(source)
                    print("Got it! Now to recognize it...")
                    answer = conversation_engine.speech_recognition(r, audio)
                    if answer is not None:
                        print_ticket(answer)
        except KeyboardInterrupt:
            pass
    else:
        while True:
            answer = conversation_engine.analyze_query(input("-> "))
            print("<- {}".format(answer))


def print_ticket(text):
    p = printer.Serial(devfile=PRINTER_DEVFILE)

    p.set(align="center", bold=True, font="b")
    p.ln()
    p.text(TICKET_HEADER)
    p.set(align="center", bold=False, font="a")
    p.ln(2)
    p.textln("La réponse de Mme Irma : ")
    p.set(align="center", bold=True, font="a")
    p.ln()
    p.textln(fill(text, width=32, use_hyphenator=hyphenator))
    p.ln(3)
    p.set(align="center", bold=False, font="b")
    p.textln(fill(
        "(Ne vous fiez pas trop à l'avis de Mme Irma, Ebull décline toute responsabilité)",
        width=40, use_hyphenator=hyphenator))
    p.ln()
    p.set(align="center", bold=True, font="b")

    p.text(TICKET_FOOTER)
    p.ln()
    p.cut()


if __name__ == '__main__':
    main()
